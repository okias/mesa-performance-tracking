#!/usr/bin/python3

# Copyright © 2021 Collabora Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

import datetime
import logging

from dateutil import parser
from influxdb_client import Point, WritePrecision

from wrappers.gitlab import GitlabConnector
from wrappers.influxdb import InfluxdbDictWriter, InfluxdbSource


def get_trace_jobs(pipeline):
    """- `pipeline` The pipeline from which to look for trace jobs
    - `return` The jobs of the pipeline responsible for replaying traces"""
    jobs = []

    print("Retrieving jobs for pipeline {}".format(pipeline.id))
    for job in pipeline.jobs.list(as_list=False, per_page=150):
        if job.status == "created":
            continue
        if "-traces-performance" in job.name:
            jobs.append(job)

    return jobs


def get_trace_frame_times(results, trace: str):
    """- `return` The frame times of a specified trace within the specified results"""

    if results["tests"][trace]["images"] is None:
        return []

    if "frame_times" not in results["tests"][trace]["images"][0]:
        return []

    frame_times = results["tests"][trace]["images"][0]["frame_times"]

    print("Found {} frame times in {}".format(len(frame_times), trace))

    return frame_times


def insert_timing(time: datetime.datetime, frame_no: int) -> int:
    """Use artificial timing to represent re-rendered values.
    This approach is mentioned in the Influx Docs, as a way to deal with duplicated data.
    https://docs.influxdata.com/influxdb/v2.0/write-data/best-practices/duplicate-points/#increment-the-timestamp

    Args:
        time (datetime.datetime): the original timing that would be used.
        frame_no (int): the number of the rendering repetition.

    Returns:
        int: the artificial timing inserted.
    """
    timestamp_ms = int(time.timestamp() * 1000)
    # 1 ns = 10e6 ms
    timestamp_ns = timestamp_ms * 1000000
    return timestamp_ns + frame_no


def extract_trace_frame_timings(
    trace, input_data, results, job, datasource, pipeline_time
):
    if not trace.endswith(".trace"):
        return

    frame_times = get_trace_frame_times(results, trace)
    if not frame_times:
        return

    # Ignore last frame, tends to be quite off
    frame_times.pop()

    # Name of the trace that has been replayed
    input_data["trace_name"] = "/".join(trace.split("@")[2:])

    print(
        "Adding %d frames for trace %s from job %s"
        % (len(frame_times), trace, job.name)
    )
    for frame_no, frame_time in enumerate(frame_times):
        # Time for a frame to hit presentation in nanoseconds
        input_data["frame_time"] = frame_time
        datasource.insert_data(
            "frame_time",
            input_data,
            insert_timing(pipeline_time, frame_no),
            write_precision=WritePrecision.NS,
        )


def upload_job_data(job, pipeline, project_path, commit, datasource):
    results = GitlabConnector.get_results(job)

    # Skip jobs with no results
    if results is None:
        return

    pipeline_time = parser.parse(pipeline.updated_at)
    input_data = {
        "job_name": job.name,
        "project_path": project_path,
        "pipeline_id": pipeline.id,
        "commit_sha": commit.short_id,
        "commit_title": commit.title,
        "job_id": job.id,
    }

    # Get frame times for apitrace traces
    for trace in results["tests"]:
        extract_trace_frame_timings(
            trace, input_data, results, job, datasource, pipeline_time
        )


def upload_data(source: InfluxdbSource, gl: GitlabConnector, pipeline):
    project = gl.project
    project_path = gl.project_path
    print("Pipeline: %s" % pipeline.web_url)

    commit = project.commits.get(pipeline.sha)

    tags = ("job_name", "trace_name", "project_path")
    fields = ("pipeline_id", "commit_sha", "commit_title", "job_id", "frame_time")
    datasource = InfluxdbDictWriter(tags, fields, source)

    for job in get_trace_jobs(pipeline):
        upload_job_data(job, pipeline, project_path, commit, datasource)


def main():
    logging.basicConfig(level=logging.DEBUG)
    source = InfluxdbSource("mesa-perf-v2")
    gl = GitlabConnector()

    last_write = source.get_last_write(measurement="frame_time", field="frame_time")
    # Uncomment below to force data extraction since the specified start range.
    # last_write = source.get_start_date()

    for pipeline in gl.pipeline_list(last_write):
        with source.writer_instance():
            upload_data(source, gl, pipeline)


if __name__ == "__main__":
    main()
