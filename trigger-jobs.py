#!/usr/bin/python3

# Copyright © 2021 Collabora Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

import gitlab
import os
import sys
import time
import dateutil.relativedelta
import datetime

GITLAB_URL = os.environ.get('GITLAB_URL', 'https://gitlab.freedesktop.org')
GITLAB_PROJECT_PATH = os.environ.get('GITLAB_PROJECT_PATH', 'mesa/mesa')
GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN', '<token unspecified>')

# Limit to 1 day
since = datetime.datetime.now(datetime.timezone.utc) - \
        dateutil.relativedelta.relativedelta(days=1)

gl = gitlab.Gitlab(url=GITLAB_URL, private_token=GITLAB_TOKEN)
project = gl.projects.get(GITLAB_PROJECT_PATH)
jobs_to_trigger = []
for pipeline in project.pipelines.list(as_list=False,
                                       per_page=50,
                                       username="marge-bot",
                                       updated_after=since.isoformat(),
                                       status="success"):
    print("Processing pipeline %d created at %s" % (pipeline.id, pipeline.created_at))
    performance_jobs = []
    for job in pipeline.jobs.list(all=True):
        if job.name.endswith('-traces-performance'):
            performance_jobs.append(job)

    if performance_jobs:
        print("Pipeline %d had these performance jobs %s" % (pipeline.id, [job.id for job in performance_jobs]))

    # Stop once we reach the first pipeline with performance jobs that have been triggered already
    if performance_jobs and \
       not [job for job in performance_jobs if job.status == 'manual']:
        print("Pipeline %d had all performance jobs triggered already, stopping" % pipeline.id)
        break

    jobs_to_trigger.extend(performance_jobs)

for job in reversed(jobs_to_trigger):
    print("Triggering job %s" % job.name)
    pjob = project.jobs.get(job.id, lazy=True)
    pjob.play()
